package com.stock.mvc.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Client;
import com.stock.mvc.entites.CommandeClient;
import com.stock.mvc.entites.LigneCommandeClient;
import com.stock.mvc.model.ModelCommandeClient;
import com.stock.mvc.model.StringResponse;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.ICommandeClientService;
import com.stock.mvc.services.ILigneCommandeClientService;

@Controller
@RequestMapping(value = "/commandeclient")
public class CommandeClientController {

	@Autowired
	private ICommandeClientService commandeService;
	
	@Autowired
	private ILigneCommandeClientService ligneCdeService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	//@Qualifier("impl")	// Pour le cas ou on a plusieurs implementations
	private ModelCommandeClient modelCommande;
	
	@RequestMapping(value = "/")
	public String index(Model model) {
		List<CommandeClient> commandeclients = commandeService.selectAll();
		if (commandeclients.isEmpty()) {
			commandeclients = new ArrayList<CommandeClient>();
		} else {
			for(CommandeClient commandeclient : commandeclients) {
				List<LigneCommandeClient> ligneCdeClt = ligneCdeService.getByIdCommande(commandeclient.getIdCommandeClient());
				commandeclient.setLigneCommandeClients(ligneCdeClt);
			}
		}
		model.addAttribute("commandeclients", commandeclients);
		modelCommande.init();
		return "commandeclient/commandeclient";
	}
	
	@RequestMapping(value = "/nouveau")
	public String nouvelleCommande(Model model) {
		List<Client> clients = clientService.selectAll();
		if (clients.isEmpty()) {
			clients = new ArrayList<Client>();
		}
		modelCommande.creerCommande();
		model.addAttribute("code", modelCommande.getCommande().getCode());
		model.addAttribute("dateCde", modelCommande.getCommande().getDateCommande());
		model.addAttribute("clients", clients);
		return "commandeclient/nouvellecommande";
	}
	
	@RequestMapping(value = "/modifier/{idCommandeClient}")
	public String modifierCommande(Model model, @PathVariable Long idCommandeClient) {
		if(idCommandeClient != null) {
			CommandeClient commande = commandeService.getById(idCommandeClient);
			List<Client> clients = clientService.selectAll();
			if(clients == null) {
				clients = new ArrayList<Client>();
			}
			modelCommande.modifierCommande(commande);
			model.addAttribute("clients", clients);
			if(commande != null) {
				model.addAttribute("code",commande.getCode());
				model.addAttribute("dateCde", commande.getDateCommande());
				model.addAttribute("cltVal", commande.getClient());
				model.addAttribute("cde",commande);
				//model.addAttribute("idCommandeClient", commande.getIdCommandeClient());
			}
		}
		
		return "commandeclient/nouvellecommande";
	}
	
	@RequestMapping(value = "/ajouterLigne", produces = "application/json")
	@ResponseBody
	public LigneCommandeClient getLigneCdeByArticle(final String codeArticle) {
		if (codeArticle == null) {
			return null;
		}
		Article article = articleService.findOne("codeArticle", codeArticle);
		if (article == null) {
			return null;
		}
		return modelCommande.ajouterLigneCommande(article);
	}
	
	@RequestMapping(value = "/afficherLigne", produces = "application/json")
	@ResponseBody
	public List<LigneCommandeClient> getListLigneCdeByCde(final Long idCommandeClient) {
		if (idCommandeClient == null) {
			return null;
		}
		List<LigneCommandeClient> ligneCommandes = ligneCdeService.getByIdCommande(idCommandeClient);
		if (ligneCommandes != null && !ligneCommandes.isEmpty()) {
			return modelCommande.afficherListLigneCommande(ligneCommandes); // Valeur de retour non utiliser mais juste pour fonctionner le getJSON
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/supprimerLigne", produces = "application/json")
	@ResponseBody
	public LigneCommandeClient supprimerLigneCommande(final Long idArticle, final Long idCommandeClient) {
		if (idArticle == null) {
			return null;
		}
		Article article = articleService.getById(idArticle);
		if (article == null) {
			return null;
		}
		return modelCommande.supprimerLigneCommande(article);
	}
	
	@RequestMapping(value = "/enregisterCommande", produces = "application/json")
	@ResponseBody
	public StringResponse enregisterCommande(final Long idClient, HttpServletRequest request) {
		if (idClient == null) {
			return null;
		}
		Client client = clientService.getById(idClient);
		if (client == null) {
			return null;
		}
		modelCommande.modifierCommande(client);
		
		CommandeClient nouvelleCommande = null;
		if (modelCommande.getCommande() != null) {
			if (modelCommande.getCommande().getIdCommandeClient() != null) {
				nouvelleCommande = commandeService.update(modelCommande.getCommande());
			} else {
				nouvelleCommande = commandeService.save(modelCommande.getCommande());
			}
		}
		if (nouvelleCommande != null) {
			Collection<LigneCommandeClient> ligneCommandes = modelCommande.getLignesCommandeClient(nouvelleCommande);
			if (ligneCommandes != null && !ligneCommandes.isEmpty()) {
				for (LigneCommandeClient ligneCommandeClient : ligneCommandes) {
					if (ligneCommandeClient.getIdLigneCdeClt() != null) {
						ligneCdeService.update(ligneCommandeClient);
					} else {
						ligneCdeService.save(ligneCommandeClient);
					}
				}
				
				/**
				 * Suppression ligne
				 */
				boolean deleteLigne = false;
				List<LigneCommandeClient> bDLigneCommandes = nouvelleCommande.getLigneCommandeClients();
				if (bDLigneCommandes != null && !bDLigneCommandes.isEmpty()) {
					for (LigneCommandeClient bDLigneCommandeClient : bDLigneCommandes) {
						if (bDLigneCommandeClient.getIdLigneCdeClt() != null) {
							deleteLigne = true;
							bb:
							for (LigneCommandeClient ligneCommandeClient : ligneCommandes) {
								if (ligneCommandeClient.getIdLigneCdeClt() != null) {
									if (ligneCommandeClient.getIdLigneCdeClt() == bDLigneCommandeClient.getIdLigneCdeClt()) {
										deleteLigne = false;
										break bb;
									}
								}
							}
						}
						if(deleteLigne) {
							ligneCdeService.remove(bDLigneCommandeClient.getIdLigneCdeClt());
						}
					}
					
				}
				
				modelCommande.init();
			}
			
			
			
		}
		return new StringResponse(request.getContextPath() + "/commandeclient/");
	}
	
	@RequestMapping(value = "/supprimer/{idCommandeClient}")
	public String supprimerCommande(Model model, @PathVariable Long idCommandeClient) {
		if(idCommandeClient != null) {
			CommandeClient commande = commandeService.getById(idCommandeClient);
			if(commande != null) {
				List<LigneCommandeClient> ligneCommandes = ligneCdeService.getByIdCommande(idCommandeClient);
				if (ligneCommandes != null && !ligneCommandes.isEmpty()) {
					for (LigneCommandeClient ligneCommandeClient : ligneCommandes) {
						ligneCdeService.remove(ligneCommandeClient.getIdLigneCdeClt());
					}
				}
				commandeService.remove(idCommandeClient);
			}
		}
		return "redirect:/commandeclient/";
	}
}
