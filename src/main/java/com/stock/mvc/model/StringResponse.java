package com.stock.mvc.model;

public class StringResponse {

	private String response;
	
	public StringResponse(String response) {
		this.response = response;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
	/*public String splitUrl(String url) {
		System.out.println(url);
		String[] arrSplit = url.split("/");
		String newUrl = "";
	    for (int i = 0; i < arrSplit.length - 1; i++) {
	    	System.out.println(arrSplit[i]);
	    	newUrl = newUrl + arrSplit[i]  + "/";
	    }
	    System.out.println(newUrl);
	    return newUrl;
	}*/
}
