package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

//import com.fasterxml.jackson.annotation.JsonIgnore;

//import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class LigneCommandeClient implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // Generer autoincrement
	private Long idLigneCdeClt;
	
	@ManyToOne	// (1 LigneCommandeClient appartient a 1 Article) et (Plusieurs LigneCommandeClient dans 1 Article)
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne	// (1 LigneCommandeClient appartient a 1 CommandeClient) et (Plusieurs LigneCommandeClient dans 1 CommandeClient)
	@JoinColumn(name = "idCommandeClient")
	private CommandeClient commandeClient;
	
	private BigDecimal quantite;
	
	private BigDecimal prixUnitaire;
	
	public LigneCommandeClient() {
		super(); // A verifier
	}

	public Long getIdLigneCdeClt() {
		return idLigneCdeClt;
	}

	public void setIdLigneCdeClt(Long id) {
		this.idLigneCdeClt = id;
	}

	//@JsonIgnore // Ignorer afin d'eviter exception de surcharge
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	@JsonIgnore // Ignorer afin d'eviter exception de surcharge
	public CommandeClient getCommandeClient() {
		return commandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public BigDecimal getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(BigDecimal prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	
}
