
 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center"
		href="javascript:void(0);">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">
			Gestion de stock
		</div>
	</a>

	<!-- Divider -->
    <hr class="sidebar-divider my-0">
    
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
    	<c:url value="/home/" var="home" />
    	<a class="nav-link" href="${home}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
    	<span><fmt:message code="common.dashbord" /></span></a>
    </li>
    
    
	<!-- Nav Item - Article -->
    <li class="nav-item">
    	<c:url value="/article/" var="article" />
    	<a class="nav-link" href="${article}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
    	<span><fmt:message code="common.article" /></span></a>
    </li>
	
	
    <!-- Nav Item - Pages Collapse Menu - Client -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseClient" aria-expanded="true" aria-controls="collapseClient">
          <i class="fas fa-fw fa-cog"></i>
          <span><fmt:message code="common.client" /></span>
        </a>
        <div id="collapseClient" class="collapse" aria-labelledby="headingClient" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><fmt:message code="common.custom.client" />:</h6>
            <c:url value="/client/" var="client" />
            <a class="collapse-item" href="${client}"><fmt:message code="common.client" /></a>
            <c:url value="/commandeclient/" var="cdeClt" />
            <a class="collapse-item" href="${cdeClt}"><fmt:message code="common.client.commande" /></a>
          </div>
        </div>
    </li>
	
	
    <!-- Nav Item - Pages Collapse Menu - Fournisseur -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFrns" aria-expanded="true" aria-controls="collapseFrns">
          <i class="fas fa-fw fa-cog"></i>
          <span><fmt:message code="common.fournisseur" /></span>
        </a>
        <div id="collapseFrns" class="collapse" aria-labelledby="headingFrns" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><fmt:message code="common.custom.fournisseur" />:</h6>
            <c:url value="/fournisseur/" var="fournisseur" />
            <a class="collapse-item" href="${fournisseur}"><fmt:message code="common.fournisseur" /></a>
            <c:url value="/commandefournisseur/" var="cdeFrns" />
            <a class="collapse-item" href="${cdeFrns}"><fmt:message code="common.fournisseur.commande" /></a>
          </div>
        </div>
    </li>
	
	
    <!-- Nav Item - Stock -->
    <li class="nav-item">
    	<c:url value="/stock/" var="stock" />
    	<a class="nav-link" href="${stock}">
        <i class="fas fa-fw fa-table"></i>
        <span><fmt:message code="common.stock" /></span></a>
    </li>
	
	
    <!-- Nav Item - Vente -->
    <li class="nav-item">
    	<c:url value="/vente/" var="vente" />
    	<a class="nav-link" href="${vente}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span><fmt:message code="common.vente" /></span></a>
    </li>
    
    <!-- Nav Item - Utilities Collapse Menu - Parametrage -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseParms" aria-expanded="true" aria-controls="collapseParms">
          <i class="fas fa-fw fa-wrench"></i>
          <span><fmt:message code="common.parametrage" /></span>
        </a>
        <div id="collapseParms" class="collapse" aria-labelledby="headingParms" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"><fmt:message code="common.parametrage" />:</h6>
            <c:url value="/utilisateur/" var="user" />
            <a class="collapse-item" href="${user}"><fmt:message code="common.parametrage.utilisateur" /></a>
            <c:url value="/category/" var="category" />
            <a class="collapse-item" href="${category}"><fmt:message code="common.parametrage.category" /></a>
          </div>
        </div>
    </li>

	<!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
    	<button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
