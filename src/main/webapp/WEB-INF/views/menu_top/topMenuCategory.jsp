
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <%@ include file="/WEB-INF/views/menu_top/topBar.jsp" %>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message code="common.category" /></h1>

		  <div class="card shadow mb-4">
		  	<div class="card-header py-3">
				<ol class="breadcrumb">
    				<li class="breadcrumb-item"><a href="<c:url value="/category/nouveau" />"><i class="fa fa-plus">&nbsp;<fmt:message code="common.ajouter" /></i></a></li>
    				<li class="breadcrumb-item"><a href="#"><i class="fa fa-download">&nbsp;<fmt:message code="common.exporter" /></i></a></li>
    				<li class="breadcrumb-item"><a href="#"><i class="fa fa-upload">&nbsp;<fmt:message code="common.importer" /></i></a></li>
    				<!-- <li class="breadcrumb-item active" aria-current="page">Data</li> -->
  				</ol>
		  	</div>
		  </div>
		  
		  <!-- debut copie -->
		  <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="category.liste" /></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th><fmt:message code="common.code" /></th>
                      <th><fmt:message code="common.designation" /></th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th><fmt:message code="common.code" /></th>
                      <th><fmt:message code="common.designation" /></th>
                    </tr>
                  </tfoot>
                  <tbody>
                  	<c:forEach items="${categorys}" var="category">
                  		<tr>
                      		<td>${category.getCode()}</td>
                      		<td>${category.getDesignation()}</td>
                      		<td>
                      			<c:url value="/category/modifier/${category.getIdCategory()}" var="urlModif" />
                      			<a href="${urlModif}"><i class="fa fa-edit"></i></a>
                      			&nbsp;|&nbsp;
                      			<a href="javascript:void(0);" data-toggle="modal" data-target="#modalCategory${category.getIdCategory()}"><i class="fa fa-trash-o"></i></a>
                      			
                      			<!-- debut copie -->
                      			<div class="modal fade" id="modalCategory${category.getIdCategory()}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    								<div class="modal-dialog">
        								<div class="modal-content">
            								<div class="modal-header">
            									<h4 class="modal-title" id="myModalLabel"><fmt:message code="common.confirm.suppression" /></h4>
            									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          											<span aria-hidden="true">&times;</span>
        										</button>
            								</div>
											<div class="modal-body">
												<fmt:message code="common.confirm.suppression.msg" />
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message code="common.annuler" /></button>
												<c:url value="/category/supprimer/${category.getIdCategory()}" var="urlSuppression" />
												<a href="${urlSuppression}" class="btn btn-danger"><i class="fa fa-trash-o"></i>&nbsp;<fmt:message code="common.confirmer" /></a>
											</div>
										</div>
										<!-- /.modal-content -->
    								</div>
    								<!-- /.modal-dialog -->
								</div>
                      			<!-- fin copie -->
                      			
                      		</td>
                    	</tr>
                  	</c:forEach>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
		  <!-- fin copie -->
		  
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <%@ include file="/WEB-INF/views/menu_top/footer.jsp" %>
      <!-- End of Footer -->

    </div>