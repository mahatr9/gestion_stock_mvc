
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
		<%@ include file="/WEB-INF/views/menu_top/topBar.jsp" %>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message code="category.nouveau" /></h1>

		  <!-- debut copie -->
		  <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="category.nouveau" /></h6>
            </div>
            <c:url value="/category/nouveau" var="urlEnregistrer" />
            <div class="card-body">
				<f:form modelAttribute="category" action="${urlEnregistrer}" method="post" >	<!-- enctype="multipart/form-data" -->
					<f:hidden path="idCategory" /> <!-- Utiliser pour Modifier/Supprimer -->
					<div class="form-group">
                    	<label><fmt:message code="common.code" /></label>
                        <f:input path="code" class="form-control" placeholder="Code" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.designation" /></label>
                        <f:input path="designation" class="form-control" placeholder="Designation" />
                    </div>
                    
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp;<fmt:message code="common.enregistrer" /></i></button>
                        <a href="<c:url value="/category/" />" class="btn btn-danger"><i class="fa fa-arrow-left">&nbsp;<fmt:message code="common.annuler" /></i></a>
                    </div>
				</f:form>
            </div>
          </div>
		  <!-- fin copie -->

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <%@ include file="/WEB-INF/views/menu_top/footer.jsp" %>
      <!-- End of Footer -->

    </div>