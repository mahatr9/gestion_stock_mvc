
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
		<%@ include file="/WEB-INF/views/menu_top/topBar.jsp" %>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
        
          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message code="commande.client.nouveau" /></h1>

		  <div class="alert alert-danger" id="notFoundMsgBlock" hidden="true">
			<fmt:message code="commande.client.not.found" />
		  </div>
		  <div class="alert alert-danger" id="clientNotSelectedMsgBlock">
			<fmt:message code="commande.client.select.client.msg.erreur" />
		  </div>
		  <div class="alert alert-danger" id="unexcpectedErrorMsgBlock">
			<fmt:message code="commande.client.select.client.unexcpected.error" />
		  </div>

		  <!-- debut copie -->
		  <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="commande.client.detail" /></h6>
            </div>
            <!-- <c:url value="/commandeclient/enregisterCommande" var="urlEnregistrer" /> -->
            <div class="card-body">
            	<form action="" method="post" enctype="multipart/form-data" role="form" >
            		<input id="idCommandeClient" value="${cde.getIdCommandeClient()}" hidden=true />
            		<textArea id="jsonn${cde.getIdCommandeClient()}" style="display:none;" >${cde.getLigneCommandeJson()}</textArea>
					<div class="form-row">
						<div class="col-md-4 mb-3">
	                    	<label><fmt:message code="common.code" /></label>
	                        <input class="form-control" placeholder="Code" id="codeCommande" value="${code}" disabled />
	                    </div>
	                    <div class="col-md-4 mb-3">
	                    	<label><fmt:message code="common.date" /></label>
	                        <input class="form-control" placeholder="Date" id="dateCommande" value="${dateCde}" disabled />
	                    </div>
	                    
	                    <div class="col-md-4 mb-3">
	                    	<label><fmt:message code="common.client" /></label>
	                    	<select class="form-control" id="listClients" itemValue="${cltVal}">
	                    		<option value="-1"><fmt:message code="commande.client.select.client" /></option> <!-- value="-1" pour obliger la selection d'un client -->
	                    		
	                    		<c:forEach items="${clients}" var="clt">
	                    			<c:choose>
		                    			<c:when test="${cltVal.getIdClient() == clt.getIdClient()}">
								            <option value="${cltVal.getIdClient()}" selected>${cltVal.getNom()}</option>
								        </c:when>
								        <c:otherwise>
								        	<option value="${clt.getIdClient()}">${clt.getNom()}</option>
								        </c:otherwise>
							        </c:choose>
	                    		</c:forEach>
	                    		
	                    	</select>
	                    </div>
	                    
                    </div>
                    <br />
                    <div class="panel-footer">
                    	<!-- <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp;<fmt:message code="common.enregistrer" /></i></button> -->
                        <button type="button" id="btnEnregistrerCommande" class="btn btn-primary"><i class="fa fa-save">&nbsp;<fmt:message code="common.enregistrer" /></i></button>
                        <a href="<c:url value="/commandeclient/" />" class="btn btn-danger"><i class="fa fa-arrow-left">&nbsp;<fmt:message code="common.annuler" /></i></a>
                    </div>
				</form>
            </div>
          </div>
		  <!-- fin copie -->
		  
		  <!-- debut copie -->
		  <!-- Detail de la commande -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="commande.client.title.nouveau" /></h6>
            </div>
            <div class="card-header py-3">
				<label><fmt:message code="common.article" /></label>
            	<input class="form-control" type="text" id="codeArticle_search" placeholder="Saisir un code article" />
		  	</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="detailN" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th><fmt:message code="common.article" /></th>
                      <th><fmt:message code="common.qte" /></th>
                      <th><fmt:message code="common.prixUnitaireTTC" /></th>
                      <th><fmt:message code="common.total" /></th>
                      <th><fmt:message code="common.actions" /></th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th><fmt:message code="common.article" /></th>
                      <th><fmt:message code="common.qte" /></th>
                      <th><fmt:message code="common.prixUnitaireTTC" /></th>
                      <th><fmt:message code="common.total" /></th>
                      <th><fmt:message code="common.actions" /></th>
                    </tr>
                  </tfoot>
                  <tbody id="detailNouvelleCommande">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
		  <!-- fin copie -->

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <%@ include file="/WEB-INF/views/menu_top/footer.jsp" %>
      <!-- End of Footer -->

    </div>