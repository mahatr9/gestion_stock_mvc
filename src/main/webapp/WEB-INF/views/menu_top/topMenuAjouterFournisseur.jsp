
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <%@ include file="/WEB-INF/views/menu_top/topBar.jsp" %>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message code="fournisseur.nouveau" /></h1>

		  <!-- debut copie -->
		  <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="fournisseur.nouveau" /></h6>
            </div>
            <c:url value="/fournisseur/nouveau" var="urlEnregistrer" />
            <div class="card-body">
				<f:form modelAttribute="fournisseur" action="${urlEnregistrer}" method="post" enctype="multipart/form-data">
					<f:hidden path="idFournisseur" /> <!-- Utiliser pour Modifier/Supprimer -->
					<f:hidden path="photo" /> <!-- Utiliser pour Modifier/Supprimer -->
					<div class="form-group">
                    	<label><fmt:message code="common.nom" /></label>
                        <f:input path="nom" class="form-control" placeholder="Nom" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.prenom" /></label>
                        <f:input path="prenom" class="form-control" placeholder="Prenom" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.adresse" /></label>
                        <f:input path="adresse" class="form-control" placeholder="Adresse" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.mail" /></label>
                        <f:input path="mail" class="form-control" placeholder="Mail" />
                    </div>
                    <div class="form-group"> 
                        <label><fmt:message code="common.photo" /></label>
                        <input type="file" name="file">
                    </div>
                    
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp;<fmt:message code="common.enregistrer" /></i></button>
                        <a href="<c:url value="/fournisseur/" />" class="btn btn-danger"><i class="fa fa-arrow-left">&nbsp;<fmt:message code="common.annuler" /></i></a>
                    </div>
				</f:form>
            </div>
          </div>
		  <!-- fin copie -->

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <%@ include file="/WEB-INF/views/menu_top/footer.jsp" %>
      <!-- End of Footer -->

    </div>