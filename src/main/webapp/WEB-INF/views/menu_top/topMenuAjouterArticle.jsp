
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
		<%@ include file="/WEB-INF/views/menu_top/topBar.jsp" %>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><fmt:message code="article.nouveau" /></h1>

		  <!-- debut copie -->
		  <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><fmt:message code="article.nouveau" /></h6>
            </div>
            <c:url value="/article/nouveau" var="urlEnregistrer" />
            <div class="card-body">
				<f:form modelAttribute="article" action="${urlEnregistrer}" method="post" enctype="multipart/form-data" role="form">
					<f:hidden path="idArticle" /> <!-- Utiliser pour Modifier/Supprimer -->
					<f:hidden path="photo" /> <!-- Utiliser pour Modifier/Supprimer -->
					<div class="form-group">
                    	<label><fmt:message code="common.code" /></label>
                        <f:input path="codeArticle" class="form-control" placeholder="CodeArticle" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.designation" /></label>
                        <f:input path="designation" class="form-control" placeholder="Designation" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.prixUnitaireHT" /></label>
                        <f:input id="prixUnitHT" path="prixUnitaireHT" class="form-control" placeholder="PrixUnitaireHT" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.tauxTva" /></label>
                        <f:input id="tauxTva" path="tauxTva" class="form-control" placeholder="TauxTva" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.prixUnitaireTTC" /></label>
                        <f:input id="prixUnitTTC" path="prixUnitaireTTC" class="form-control" placeholder="PrixUnitaireTTC" />
                    </div>
                    <div class="form-group">
                    	<label><fmt:message code="common.parametrage.category" /></label>
                        <f:select class="form-control" path="category.idCategory" items="${categories}" itemLabel="code" itemValue="idCategory" />
                    </div>
                    <div class="form-group">
                        <label><fmt:message code="common.photo" /></label>
                        <input type="file" name="file">
                    </div>
                    
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save">&nbsp;<fmt:message code="common.enregistrer" /></i></button>
                        <a href="<c:url value="/article/" />" class="btn btn-danger"><i class="fa fa-arrow-left">&nbsp;<fmt:message code="common.annuler" /></i></a>
                    </div>
				</f:form>
            </div>
          </div>
		  <!-- fin copie -->

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <%@ include file="/WEB-INF/views/menu_top/footer.jsp" %>
      <!-- End of Footer -->

    </div>