var context = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
var urlinit = window.location.protocol + "//" + window.location.host + context;
var detailHtmlSave = "";
var arrayHtml = [[]];
delete arrayHtml[0];
arrayHtml.splice(0, 1);

$(document).ready(function() {
	$("#notFoundMsgBlock").hide();
	$("#clientNotSelectedMsgBlock").hide();
	$("#unexcpectedErrorMsgBlock").hide();
	
	var incr = 0;
	var idCommande = $("#idCommandeClient").val();
	if(idCommande) {
		searchListArticle(idCommande);
	}
	
	$("#codeArticle_search").on("keypress", function(e) {
		if(e.which == '13') {
			var codeArticle = $("#codeArticle_search").val();
			if(verifierClient() && codeArticle) {
				/*if(idCommande && incr == 0) {
					$("#detailNouvelleCommande").html("");
					incr++;
				}*/
				searchArticle(codeArticle, idCommande);
				/*if(idCommande) {
					searchListArticle(idCommande);
				}*/
			}
		}
	});
	
	$("#listClients").on("change", function(e) {
		if(verifierClient()) {
			$("#clientNotSelectedMsgBlock").hide("slow", function() { $("#clientNotSelectedMsgBlock").hide(); });
			//creeCommande($("#listClients option:selected").val());
		}
	});
	
	$("#btnEnregistrerCommande").on("click", function() {
		var idClient = $("#listClients option:selected").val();
		var url = urlinit + "/commandeclient/enregisterCommande";
		$.getJSON(url, {
			idClient: idClient,
			ajax: true
		}, 
		function(data) {
			if (data) {
				var url = urlinit + "/commandeclient/";
				window.location = url;
			}
		});
	});
	
	/*$("#notFoundMsgBlock").hide();
	$("#clientNotSelectedMsgBlock").hide();
	$("#unexcpectedErrorMsgBlock").hide();*/
});

function verifierClient() {
	var idClient = $("#listClients option:selected").val();
	if(idClient) {
		if(idClient === '-1') {
			$("#clientNotSelectedMsgBlock").show("slow", function() { $("#clientNotSelectedMsgBlock").show(); });
			return false;
		} 
		return true;
	}
}

function refreshPagination(idTable, idTbody, valhtml) {
	//clear and destroy the table first
	idTable.DataTable().clear();
	idTable.DataTable().destroy();

	//in my case,i will use the append method to insert my HTML Blocks of code first 
	idTbody.html(valhtml);

	//and finally 
	idTable.DataTable();
}

function updateDetailCommande(idCommande) {
	var json = $.parseJSON($("#json" + idCommande).text());
	var detailHtml = "";
	if(json) {
		for(var index = 0; index < json.length; index++) {
			detailHtml += 
						"<tr>" + 
							"<td>" + json[index].article.codeArticle + "</td>" + 
							"<td>" + json[index].quantite + "</td>" + 
							"<td>" + json[index].prixUnitaire + "</td>" + 
							"<td>" + (json[index].quantite * json[index].prixUnitaire) + "</td>" + 
							/*"<td></td>"*/
						"</tr>";
		}
		refreshPagination($("#detail"), $("#detailCommande"), detailHtml);
	}
};

function searchArticle(codeArticle, idCommande) {
	if(codeArticle) {
		var url = urlinit + "/commandeclient/ajouterLigne";
		$.getJSON(url, {
			codeArticle: codeArticle,
			ajax: true
		},
		function(data) {
			adddetailCommande(data, idCommande);
		}).fail(function(jqxhr, textStatus, error) {
		    var err = textStatus + ", " + error;
			$("#notFoundMsgBlock").show("slow", function() { $("#notFoundMsgBlock").show(); });
		});
	}
}

function searchListArticle(idCommande) {
	var json = $.parseJSON($("#jsonn" + idCommande).text());
	if(json) {
		for(var index = 0; index < json.length; index++) {
			adddetailCommande(json[index], idCommande);
		}
	}
	
	if(idCommande) {
		var detailHtml = ""; 
		var url = urlinit + "/commandeclient/afficherLigne";
		$.getJSON(url, {
			idCommandeClient: idCommande,
			ajax: true
		},
		function(data) {
			console.log("afficherLigne OK");
			
			/*for (var i = 0; i < arrayHtml.length; i++) {
	        	for (var j = 0; j < arrayHtml[i].length; j++) {
	        		console.log(arrayHtml[i][j] + " || ");
	        	}
	        	console.log("<br>");
			}*/
			
			//adddetailCommande(data, idCommande);
		}).fail(function(jqxhr, textStatus, error) {
		    /*var err = textStatus + ", " + error;
		    alert( "Request Failed: " + err );*/
			$("#notFoundMsgBlock").show("slow", function() { $("#notFoundMsgBlock").show(); });
		});
	}
	
}

function adddetailCommande(data, idCommande) {
	if(data) {
		var detailHtml = "";
		var total = data.quantite * data.prixUnitaire;
		
		// Gerer le cas de creation d'une commande
		if(!idCommande) {
			idCommande = "";
		}
		
		detailHtml = 
			"<tr id='ligne" + data.article.idArticle + idCommande + "'>" + 
				"<td>" + data.article.codeArticle + "</td>" + 
				"<td id='qte" + data.article.idArticle + idCommande + "'>" + data.quantite + "</td>" + 
				"<td>" + data.prixUnitaire + "</td>" + 
				"<td id='total" + data.article.idArticle + idCommande + "'>" + total + "</td>" + 
				"<td><button class='btn btn-link' onclick='supprimerLigneCommande(" + data.article.idArticle + ", " + idCommande + ")'><i class='fa fa-trash-o'></i></button></td>" + 
			"</tr>";
		
		if($("#qte" + data.article.idArticle + idCommande).length > 0 && $("#total" + data.article.idArticle + idCommande).length > 0) {
			$("#qte" + data.article.idArticle + idCommande).text(data.quantite);
			$("#total" + data.article.idArticle + idCommande).text(total);
			
			// Remplacer la quantite et le total
			for (var i = 0; i < arrayHtml.length; i++) {
	        	if(arrayHtml[i][0] == "ligne" + data.article.idArticle + idCommande) {
	        		console.log("replace " + arrayHtml[i][0]);
	        		arrayHtml[i][1] = detailHtml;
	        		i = arrayHtml.length; // Sortir de la boucle
	        	}
			}
			
		} else {
			arrayHtml.push(["ligne" + data.article.idArticle + idCommande, detailHtml]);
		}
		
		detailHtmlSave = "";
		for (var i = 0; i < arrayHtml.length; i++) {
			detailHtmlSave += arrayHtml[i][1];
		}
		refreshPagination($("#detailN"), $("#detailNouvelleCommande"), detailHtmlSave);
		
		$("#notFoundMsgBlock").hide("slow", function() { $("#notFoundMsgBlock").hide(); });
		$("#codeArticle_search").val("");
	}
}

function supprimerLigneCommande(idArticle, idCommande) {
	// Gerer le cas de creation d'une commande
	if(!idCommande) {
		idCommande = "";
	}
	
	if($("#ligne" + idArticle + idCommande).length > 0) {
		var url = urlinit + "/commandeclient/supprimerLigne";
		$.getJSON(url, {
			idArticle: idArticle,
			idCommandeClient: idCommande,
			ajax: true
		},
		function(data) {
			if(data) {
				for (var i = 0; i < arrayHtml.length; i++) {
		        	if(arrayHtml[i][0] == "ligne" + idArticle + idCommande) {
		        		console.log("delete " + arrayHtml[i][0]);
		        		delete arrayHtml[i];
		        		arrayHtml.splice(i, 1);
		        		i = arrayHtml.length; // Sortir de la boucle
		        	}
				}
				
				detailHtmlSave = "";
				for (var i = 0; i < arrayHtml.length; i++) {
					detailHtmlSave += arrayHtml[i][1];
				}
				
				$("#ligne" + idArticle + idCommande).hide("slow", function() { $("#ligne" + idArticle + idCommande).remove(); });
				
				refreshPagination($("#detailN"), $("#detailNouvelleCommande"), detailHtmlSave);
			}
		});
	}
}
